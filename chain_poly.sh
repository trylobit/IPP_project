#!/bin/bash

GREEN="\033[0;32m"
RED="\033[0;31m"
DEFAULT="\033[0m"

# checking if arguments are correct
if [[ $# != 2 ]]; then
    echo -e "${RED}ERROR: Invalid number of arguments${DEFAULT}"
    exit 1
fi

PROG=$1
DEST=$2

if [[ ! -x "$PROG" ]]; then
    echo -e "${RED}ERROR: The file isn't executable${DEFAULT}"
    exit 1
fi

if [[ ! -d "$DEST" ]]; then
    echo -e "${RED}ERROR: Directory needed.${DEFAULT}"
    exit 1
fi

FILE=$(grep -l "^START" $DEST/*)
STOP=0
touch in.tmp # temporary file for input/output manipulation

while [[ $STOP == 0 ]]; do
    egrep -v "^START|^STOP|^FILE" "$FILE" >>in.tmp
    ./$PROG <in.tmp >out.tmp
    rm in.tmp
    mv out.tmp in.tmp
    if egrep -q "^STOP" "$FILE"; then
        STOP=1
    else
        NEW_FILE=$(grep "^FILE" $FILE)
        FILE=$DEST"/"${NEW_FILE#FILE }
    fi
done

cat in.tmp
rm in.tmp

exit 0
