/** @file
   Polynomial class interface

   @author Jakub Pawlewicz <pan@mimuw.edu.pl>, TODO
   @copyright University of Warsaw
   @date 2017-04-24, TODO
*/

#ifndef __POLY_H__
#define __POLY_H__

#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

/** Polynomial coefficient type */
typedef long poly_coeff_t;

/** Polynomial exponent type */
typedef int poly_exp_t;

struct MonoList;

/**
 * Structure representing polynomial
 * If polynomial is constant then first = NULL
 * Polynomial is then equal to coeff
 * Otherwise coeff = 0 and we don't consider it
 */
typedef struct Poly
{
    poly_coeff_t coeff; ///< polynomial coefficient
    struct MonoList *first; ///< pointer to the list of monomials which polynomial consists of
} Poly;

/**
 * Structure representing monomial
 * Monomial is of figure `p * x^e`.
 * Coefficient `p` can be a polynomial.
 * It will be treated as a polynomial over another variable (not over x).
 */ 
typedef struct Mono
{
    Poly p; ///< coefficient
    poly_exp_t exp; ///< exponent
} Mono;

/**
 * Monomial list
 */
typedef struct MonoList
{
    struct Mono mono; ///< monomial
    struct MonoList *next; ///< pointer to next monomial
} MonoList;

/**
 * Creates polynomial, which is a coefficient.
 * @param[in] c : coefficient value
 * @return polynomial
 */
static inline Poly PolyFromCoeff(poly_coeff_t c)
{
    return (Poly) {.coeff = c, .first = NULL};
}

/**
 * Creates polynomial which is equal to zero.
 * @return polynomial
 */
static inline Poly PolyZero()
{
    return (Poly) {.coeff = 0, .first = NULL};
}

/**
 * Creates monomial `p * x^e`.
 * Takes the ownership of all members of structure pointed by @p p.
 * @param[in] p : polynomial - monomial coefficient
 * @param[in] e : exponent
 * @return monomial `p * x^e`
 */
static inline Mono MonoFromPoly(const Poly *p, poly_exp_t e)
{
    return (Mono) {.p = *p, .exp = e};
}

/**
 * Checks if polynomial is coefficient.
 * @param[in] p : polynomial
 * @return Is polynomial coefficient?
 */
static inline bool PolyIsCoeff(const Poly *p)
{
    return p->first == NULL;
}

/**
 * Checks if polynomial is equal to zero.
 * @param[in] p : polynomial
 * @return Is polynomial equal to zero?
 */
static inline bool PolyIsZero(const Poly *p)
{
    return p->first == NULL && p->coeff == 0;
}

/**
 * Deletes polynomial from memory
 * @param[in] p : polynomial
 */
void PolyDestroy(Poly *p);

/**
 * Deletes monomial form memory.
 * @param[in] m : monomial
 */
static inline void MonoDestroy(Mono *m)
{
    PolyDestroy(&m->p);
}

/**
 * Creates a full, deep copy of polynomial
 * @param[in] p : polynomial
 * @return cloned polynomial
 */
Poly PolyClone(const Poly *p);

/**
 * Creates a full, deep copy of monomial
 * @param[in] m : monomial
 * @return cloned monomial
 */
static inline Mono MonoClone(const Mono *m)
{
    return (Mono) {.p = PolyClone(&m->p), .exp = m->exp};
}

/**
 * Adds two polynomials.
 * @param[in] p : polynomial
 * @param[in] q : polynomial
 * @return `p + q`
 */
Poly PolyAdd(const Poly *p, const Poly *q);

/**
 * Sums a list of monomials and creates polynomial of them.
 * Takes ownership of the content of @p monos array.
 * @param[in] count : length of monos array
 * @param[in] monos : monomials array
 * @return polynomial which is a sum of monomials
 */
Poly PolyAddMonos(unsigned count, const Mono monos[]);

/**
 * Multiplies two polynomials.
 * @param[in] p : polynomial
 * @param[in] q : polynomial
 * @return `p + q`
 */
Poly PolyMul(const Poly *p, const Poly *q);

/**
 * Returns negated polynomial.
 * @param[in] p : polynomial
 * @return `-p
 */
Poly PolyNeg(const Poly *p);

/**
 * Substracts polynomial from polynomial
 * @param[in] p : polynomial
 * @param[in] q : polynomial
 * @return `p - q`
 */
Poly PolySub(const Poly *p, const Poly *q);

/**
 * Returns degree of polynomial with reference to given variable (-1 for polynomial
 * equal to zero).
 * Variables are indexed starting from zero.
 * Variable with index 0 is a main variable of this polynomial.
 * Bigger indices correspond to polynomial variables located in
 * coefficients.
 * @param[in] p : polynomial
 * @param[in] var_idx : variable index
 * @return degree of polynomial @p p with reference to variable of index @p var_idx
 */
poly_exp_t PolyDegBy(const Poly *p, unsigned var_idx);

/**
 * Returns polyomial degree (-1 for polynomial equal to zero).
 * @param[in] p : polynomial
 * @return degree of polynomial @p p
 */
poly_exp_t PolyDeg(const Poly *p);

/**
 * Checks for polynomials equality
 * @param[in] p : polynomial
 * @param[in] q : polynomial
 * @return `p == q`
 */
bool PolyIsEq(const Poly *p, const Poly *q);

/**
 * Calculates polynomial value in point @p x.
 * Inserts @p x as value of the first polynomial variable.
 * Result can be polynomial if polynomial coefficients are polynomials
 * and variables indices are dereased by one then.
 * Formally speaking for polynomial @f$p(x_0, x_1, x_2, \ldots)@f$ result is
 * polynomial @f$p(x, x_0, x_1, \ldots)@f$.
 * @param[in] p : polynomial
 * @param[in] x : point to calculate value at
 * @return @f$p(x, x_0, x_1, \ldots)@f$
 */
Poly PolyAt(const Poly *p, poly_coeff_t x);

#endif /* __POLY_H__ */
