/** @file
   Polynomial class implementation

   @author Norbert Siwek
*/

#include "poly.h"

/*====== SECTION WITH HELPER FUNCTIONS IMPLEMENTATION ======*/

/**
 * Returns bigger of coefficients
 * @param e1 : coefficient
 * @param e2 : coefficient
 * @return max(e1, e2)
 */
static inline poly_exp_t MaxExp(poly_exp_t e1, poly_exp_t e2)
{
    return e1 > e2 ? e1 : e2;
}

/**
 * Creates monomials list element from monomial
 * @param m : monomial
 * @return MonoList from @p m
 */
static inline MonoList MonoListFromMono(const Mono *m)
{
    MonoList ml = (MonoList) {.mono = *m, .next = NULL};
    return ml;
}

/**
 * Deletes monomials list
 * @param ml : monomials list
 */
static void MonoListDestroy(MonoList *ml)
{
    if (ml != NULL)
    {
        MonoListDestroy(ml->next);
        MonoDestroy(&ml->mono);
        free(ml);
    }
}

/**
 * Combines two ascending lists with reference to exponents
 * @param ml : monomials list
 * @param ml2 : monomials list
 * @return ascending list with reference to exponent
 */
static MonoList *MonoListAddSorted(MonoList *ml, MonoList *ml2)
{
    if (ml == NULL) return ml2;
    if (ml2 == NULL) return ml;
    MonoList *dummy = (MonoList *) malloc(sizeof(MonoList));
    MonoList *last = dummy;
    while (ml != NULL && ml2 != NULL)
    {
        if (ml->mono.exp < ml2->mono.exp)
        {
            last->next = ml;
            last = last->next;
            ml = ml->next;
        } else
        {
            last->next = ml2;
            last = last->next;
            ml2 = ml2->next;
        }
    }
    if (ml == NULL)
    {
        last->next = ml2;
    } else
    {
        last->next = ml;
    }
    MonoList *first = dummy->next;
    free(dummy);
    return first;
}

/**
 * Creates deep copy of monomials list
 * @param ml : monomials list
 * @return cloned monomials list
 */
static MonoList *MonoListClone(MonoList *ml)
{
    if (ml == NULL) return NULL;
    MonoList *clone = (MonoList *) malloc(sizeof(MonoList));
    Mono m = MonoClone(&ml->mono);
    clone->mono = m;
    clone->next = MonoListClone(ml->next);
    return clone;
}

/**
 * Checks for monomials equality
 * @param m1 : monomials
 * @param m2 : monomials
 * @return `m1 == m2`
 */
static inline bool MonoIsEq(const Mono *m1, const Mono *m2)
{
    if (m1->exp != m2->exp) return false;
    return PolyIsEq(&m1->p, &m2->p);
}

/**
 * Function used for comparing monomials in quicksort
 * @param a : pointer to monomial
 * @param b : pointer to monomial
 * @return `a-> exp - b->exp`
 */
static poly_exp_t MonoCmpFunc(const void *a, const void *b)
{
    return (((Mono *) a)->exp - ((Mono *) b)->exp);
}

/**
 * Function that multiplies polynomial by coefficient
 * @param p : polynomial
 * @param c : coefficient
 */
static void PolyMulByCoeff(Poly *p, poly_coeff_t c)
{
    if (p == NULL) return;
    if (PolyIsCoeff(p))
    {
        p->coeff *= c;
    } else
    {
        MonoList *ml = p->first;
        while (ml != NULL)
        {
            PolyMulByCoeff(&ml->mono.p, c);
            ml = ml->next;
        }
    }
}

static void PolyCleanZeros(Poly *p);

/**
 * Adds coeficient to polynomial
 * @param p : polynomial
 * @param c : coefficient
 */
static void PolyAddCoeff(Poly *p, poly_coeff_t c)
{
    if (PolyIsCoeff(p))
    {
        p->coeff += c;
    } else
    {
        if (p->first->mono.exp == 0)
        {
            PolyAddCoeff(&p->first->mono.p, c);
        } else
        {
            Poly p_coeff = PolyFromCoeff(c);
            Mono m_coeff = MonoFromPoly(&p_coeff, 0);
            MonoList *ml_coeff = (MonoList *) malloc(sizeof(MonoList));
            *ml_coeff = MonoListFromMono(&m_coeff);
            ml_coeff->next = p->first;
            p->first = ml_coeff;
        }
    }
    PolyCleanZeros(p);
}

/**
 * Multiplies polynomial and monomial
 * @param p : polynomial
 * @param m : monomial
 */
static void PolyMulByMono(Poly *p, Mono *m)
{
    MonoList *ml = p->first;
    while (ml != NULL)
    {
        ml->mono.exp += m->exp;
        Poly new_p = PolyMul(&ml->mono.p, &m->p);
        PolyDestroy(&ml->mono.p);
        ml->mono.p = new_p;
        ml = ml->next;
    }
}

/** Function that cleans polynomial from polynomials equal to zero and sorts polynomial
 * @param p : polynomial
 */
static void PolyCleanZeros(Poly *p)
{
    if (p == NULL) return;
    if (PolyIsCoeff(p)) return;
    MonoList *dummy = (MonoList *) malloc(sizeof(MonoList));
    dummy->next = p->first;
    MonoList *ml = dummy;
    // kasowanie zer z wielomianu
    while (ml->next != NULL)
    {
        if (PolyIsZero(&ml->next->mono.p))
        {
            MonoList *new_next = ml->next->next;
            MonoDestroy(&ml->next->mono);
            free(ml->next);
            ml->next = new_next;
        } else
        {
            ml = ml->next;
        }

    }
    p->first = dummy->next;
    free(dummy);
    // I equalize polynomial level
    // e.g. if it is a polynomial consisting of polynomials equal to zero
    // the result will be polynomial equal to zero
    if (p->first != NULL)
    {
        if (p->first->next == NULL && PolyIsCoeff(&p->first->mono.p))
        {
            if (PolyIsZero(&p->first->mono.p))
            {
                MonoListDestroy(p->first);
                p->first = NULL;
            } else if (p->first->mono.exp == 0)
            {
                p->coeff = p->first->mono.p.coeff;
                MonoListDestroy(p->first);
                p->first = NULL;
            }
        }
    }
}

/**
 * Adds to polynomials but doesn't copy them, it modifies them
 * @param p : polynomial
 * @param q : polynomial
 * @return `p + q`
 */
static Poly PolyAddNotConst(Poly *p, Poly *q)
{
    if (PolyIsCoeff(p))
    {
        PolyAddCoeff(q, p->coeff);
        return *q;
    } else if (PolyIsCoeff(q))
    {
        PolyAddCoeff(p, q->coeff);
        return *p;
    }
    p->first = MonoListAddSorted(p->first, q->first);
    MonoList *ml = p->first;
    // sums monomials of equal exponents
    while (ml != NULL && ml->next != NULL)
    {
        if (ml->mono.exp == ml->next->mono.exp)
        {
            Poly new_poly = PolyAddNotConst(&ml->mono.p, &ml->next->mono.p);
            poly_exp_t exp = ml->mono.exp;
            ml->mono = MonoFromPoly(&new_poly, exp);
            MonoList *new_next = ml->next->next;
            free(ml->next);
            ml->next = new_next;
        } else
        {
            ml = ml->next;
        }
    }
    PolyCleanZeros(p);
    return *p;
}

/*====== BELOW ARE IMPLEMENTATIONS OF FUNCTIONS FROM poly.h ======*/

void PolyDestroy(Poly *p)
{
    MonoListDestroy(p->first);
}

Poly PolyClone(const Poly *p)
{
    return (Poly) {.coeff = p->coeff, .first = MonoListClone(p->first)};
}

Poly PolyAdd(const Poly *p, const Poly *q)
{
    Poly p_copy = PolyClone(p);
    Poly q_copy = PolyClone(q);
    return PolyAddNotConst(&p_copy, &q_copy);
}

Poly PolyAddMonos(unsigned count, const Mono monos[])
{
    if (count == 0) return PolyZero();
    // sorts given monomials ascending
    Mono monos_sort[count];
    for (unsigned i = 0; i < count; i++)
    {
        monos_sort[i] = MonoFromPoly(&monos[i].p, monos[i].exp);
    }

    qsort(monos_sort, count, sizeof(Mono), MonoCmpFunc);
    // creates a monomials list from monomials
    MonoList *dummy = (MonoList *) malloc(sizeof(MonoList));
    MonoList *ml = dummy;
    for (unsigned i = 0; i < count; i++)
    {
        MonoList *new_elem = (MonoList *) malloc(sizeof(MonoList));
        *new_elem = MonoListFromMono(&monos_sort[i]);
        ml->next = new_elem;
        ml = ml->next;
    }
    ml = dummy->next;
    free(dummy);
    // creates polynomial from monomials list
    Poly p = (Poly) {.coeff = 0, .first = ml};

    // sums monomials with same exponents
    ml = p.first;
    while (ml != NULL && ml->next != NULL)
    {
        if (ml->mono.exp == ml->next->mono.exp)
        {
            Poly new_poly = PolyAdd(&ml->mono.p, &ml->next->mono.p);
            poly_exp_t exp = ml->mono.exp;
            MonoDestroy(&ml->mono);
            MonoDestroy(&ml->next->mono);
            ml->mono = MonoFromPoly(&new_poly, exp);
            MonoList *new_next = ml->next->next;
            free(ml->next);
            ml->next = new_next;
        } else
        {
            ml = ml->next;
        }
    }
    PolyCleanZeros(&p);
    return p;
}

Poly PolyMul(const Poly *p, const Poly *q)
{
    if (PolyIsCoeff(p))
    {
        if (p->coeff == 0)
        {
            return PolyZero();
        }
        Poly poly_mul = PolyClone(q);
        PolyMulByCoeff(&poly_mul, p->coeff);
        return poly_mul;
    }
    if (PolyIsCoeff(q))
    {
        if (q->coeff == 0)
        {
            return PolyZero();
        }
        Poly poly_mul = PolyClone(p);
        PolyMulByCoeff(&poly_mul, q->coeff);
        return poly_mul;
    }
    // multiplies p by each monomials which q consists of
    // and sum the results 
    Poly poly_mul = PolyZero();
    MonoList *ml = q->first;
    while (ml != NULL)
    {
        Poly new_p = PolyClone(p);
        PolyMulByMono(&new_p, &ml->mono);
        Poly new_mul = PolyAdd(&poly_mul, &new_p);
        PolyDestroy(&new_p);
        PolyDestroy(&poly_mul);
        poly_mul = new_mul;
        ml = ml->next;
    }
    return poly_mul;
}

Poly PolyNeg(const Poly *p)
{
    Poly clone = PolyClone(p);
    PolyMulByCoeff(&clone, -1);
    return clone;
}

Poly PolySub(const Poly *p, const Poly *q)
{
    // substraction is adding negated polynomial
    Poly p_clone = PolyClone(p);
    Poly q_neg = PolyNeg(q);
    Poly result = PolyAdd(&p_clone, &q_neg);
    PolyDestroy(&p_clone);
    PolyDestroy(&q_neg);
    return result;
}

poly_exp_t PolyDegBy(const Poly *p, unsigned var_idx)
{
    if (p == NULL) return -1;
    if (PolyIsCoeff(p))
    {
        if (var_idx == 0)
        {
            return -(p->coeff == 0);
        } else
        {
            return PolyIsZero(p) ? -1 : 0;
        }
    }
    if (var_idx == 0)
    {
        // if I am deep enough I find the freates exponent
        poly_exp_t deg = p->first->mono.exp;
        MonoList *ml = p->first->next;
        while (ml != NULL)
        {
            deg = MaxExp(deg, ml->mono.exp);
            ml = ml->next;
        }
        return deg;
    } else
    {
        // if I am too deep I take maximum of degby each of monomials
        poly_exp_t deg = PolyDegBy(&p->first->mono.p, var_idx - 1);
        MonoList *ml = p->first->next;
        while (ml != NULL)
        {
            deg = MaxExp(deg, PolyDegBy(&ml->mono.p, var_idx - 1));
            ml = ml->next;
        }
        return deg;
    }
}

poly_exp_t PolyDeg(const Poly *p)
{
    // I look for greates degree polynomial recursively
    if (p == NULL) return -1;
    if (PolyIsZero(p)) return -1;
    poly_exp_t deg = 0;
    MonoList *ml = p->first;
    while (ml != NULL)
    {
        deg = MaxExp(deg, PolyDeg(&ml->mono.p) + ml->mono.exp);
        ml = ml->next;
    }
    return deg;
}

bool PolyIsEq(const Poly *p, const Poly *q)
{
    if (p == NULL) return q == NULL;
    if (q == NULL) return false;
    if (PolyIsZero(p))
    {
        return PolyIsZero(q);
    }
    if (PolyIsZero(q)) return false;
    if (PolyIsCoeff(p))
    {
        return PolyIsCoeff(q) && q->coeff == p->coeff;
    }
    if (PolyIsCoeff(q)) return false;

    // Comparing monomials which polynomial consists of
    // (they are sorted ascending with reference to exponent)
    MonoList *p_list = p->first;
    MonoList *q_list = q->first;
    while (p_list != NULL && q_list != NULL)
    {
        if (!MonoIsEq(&p_list->mono, &q_list->mono)) return false;
        p_list = p_list->next;
        q_list = q_list->next;
    }
    return p_list == NULL && q_list == NULL;
}

Poly PolyAt(const Poly *p, poly_coeff_t x)
{
    if (PolyIsCoeff(p)) return PolyClone(p);
    Poly poly_at = PolyZero();
    Poly p_clone = PolyClone(p);
    poly_exp_t current_exp = 0;
    poly_coeff_t current_coeff = 1;
    MonoList *ml = p_clone.first;
    // I calculate coefficients that each polynomial inside of monomial should be multiplied by
    // then I sum the results
    while (ml != NULL)
    {
        while (current_exp < ml->mono.exp)
        {
            current_exp++;
            current_coeff *= x;
        }
        PolyMulByCoeff(&ml->mono.p, current_coeff);

        Poly new_p = PolyAdd(&poly_at, &ml->mono.p);
        PolyDestroy(&poly_at);
        poly_at = new_p;

        ml = ml->next;
    }
    PolyDestroy(&p_clone);
    return poly_at;
}

