/** @file
   Polynomials calculator implementation

   @author Norbert Siwek
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "poly.h"

/* ====== STACK ======= */

/**
 * Structure representing a stack of polynomials
 *  */
typedef struct PolyStack
{
    Poly p; ///< polynomial
    struct PolyStack *next; ///< pointer to next stack element
} PolyStack;

/**
 * Global variable pointing to polynomials stack
 */
static PolyStack *stack;

/**
 * procedure which puts polynomial p on stack
 * @param p : polynomial
 */
static void StackPush(const Poly p)
{
    PolyStack *new_el = (PolyStack *) malloc(sizeof(PolyStack));
    new_el->p = p;
    new_el->next = stack;
    stack = new_el;
}

/**
 * Function that pops polynomial from top of the stack
 * @return polynomial
 */
static Poly StackPop()
{
    PolyStack *new_top = stack->next;
    Poly p = stack->p;
    free(stack);
    stack = new_top;
    return p;
}

/**
 * Function that returns polynomial from top of the stack
 * @return polynomial
 */
static Poly StackTop()
{
    return stack->p;
}

/**
 * Function that returns second polynomial from top of the stack
 * @return polynomial
 */
static Poly StackTopSecond()
{
    return stack->next->p;
}

/**
 * Function which checks if stack is empty
 * @return `is stack empty?`
 */
static inline bool StackIsEmpty()
{
    return stack == NULL;
}

/**
 * Function that checks if stack has two elements
 * @return `does stack have 2 el?`
 */
static inline bool StackHasTwoEl()
{
    return stack == NULL ? false : stack->next != NULL;
}

/**
 * procedure freeing the stack
 */
static void StackFree()
{
    if (stack != NULL)
    {
        PolyStack *el = stack;
        stack = stack->next;
        StackFree();
        PolyDestroy(&el->p);
        free(el);
    }
}

/* ====== COMMANDS IMPLEMENTATION ====== */

static inline void Zero()
{
    StackPush(PolyZero());
}

static inline bool IsCoeff()
{
    const Poly p = StackTop();
    return PolyIsCoeff(&p);
}

static inline bool IsZero()
{
    const Poly p = StackTop();
    return PolyIsZero(&p);
}

static inline void Clone()
{
    const Poly p = StackTop();
    StackPush(PolyClone(&p));
}

static void Add()
{
    Poly p1 = StackPop();
    Poly p2 = StackPop();
    Poly res = PolyAdd(&p1, &p2);
    PolyDestroy(&p1);
    PolyDestroy(&p2);
    StackPush(res);
}

static void Mul()
{
    Poly p1 = StackPop();
    Poly p2 = StackPop();
    Poly res = PolyMul(&p1, &p2);
    PolyDestroy(&p1);
    PolyDestroy(&p2);
    StackPush(res);
}

static void Neg()
{
    Poly p = StackPop();
    Poly res = PolyNeg(&p);
    PolyDestroy(&p);
    StackPush(res);
}

static void Sub()
{
    Poly p1 = StackPop();
    Poly p2 = StackPop();
    Poly res = PolySub(&p1, &p2);
    PolyDestroy(&p1);
    PolyDestroy(&p2);
    StackPush(res);
}

static bool IsEq()
{
    const Poly p1 = StackTop();
    const Poly p2 = StackTopSecond();
    return PolyIsEq(&p1, &p2);
}

static inline poly_exp_t Deg()
{
    const Poly p = StackTop();
    return PolyDeg(&p);
}

static inline poly_exp_t DegBy(unsigned var_idx)
{
    const Poly p = StackTop();
    return PolyDegBy(&p, var_idx);
}

static void At(long x)
{
    Poly p = StackPop();
    Poly res = PolyAt(&p, x);
    PolyDestroy(&p);
    StackPush(res);
}

static void PrintMono(const Mono *m);

static void PrintPoly(const Poly *p)
{
    bool plus = false;
    if (PolyIsCoeff(p))
    {
        printf("%ld", p->coeff);
        return;
    }
    MonoList *ml = p->first;
    while (ml != NULL)
    {
        if (plus)
        {
            printf("+");
        } else
        {
            plus = true;
        }
        PrintMono(&ml->mono);
        ml = ml->next;
    }
}

static void PrintMono(const Mono *m)
{
    printf("(");
    PrintPoly(&m->p);
    printf(",%d)", m->exp);
}

static void Print()
{
    const Poly p = StackTop();
    PrintPoly(&p);
    printf("\n");
}

static inline void Pop()
{
    Poly p = StackPop();
    PolyDestroy(&p);
}

/* ====== ERRORS HANDLING ====== */

static inline void PrintErrStackUnderflow(int r)
{
    fprintf(stderr, "ERROR %d STACK UNDERFLOW\n", r);
}

static inline void PrintErr(int r, int c)
{
    fprintf(stderr, "ERROR %d %d\n", r, c - 1);
}

static inline void PrintErrWrongCommand(int r)
{
    fprintf(stderr, "ERROR %d WRONG COMMAND\n", r);
}

static inline void PrintErrWrongValue(int r)
{
    fprintf(stderr, "ERROR %d WRONG VALUE\n", r);
}


static inline void PrintErrWrongVariable(int r)
{
    fprintf(stderr, "ERROR %d WRONG VARIABLE\n", r);
}

/* ====== PARSING ====== */

#define ZERO "ZERO"
#define IS_COEFF "IS_COEFF"
#define IS_ZERO "IS_ZERO"
#define CLONE "CLONE"
#define ADD "ADD"
#define MUL "MUL"
#define NEG "NEG"
#define SUB "SUB"
#define IS_EQ "IS_EQ"
#define DEG "DEG"
#define DEG_BY "DEG_BY"
#define AT "AT"
#define PRINT "PRINT"
#define POP "POP"

#define MAX_COMMAND_LENGTH (8 + 1)

static inline void ClearLine()
{
    int c = getchar();
    while (c != '\n' && c != EOF)
    {
        c = getchar();
    }
}

static inline int nextchar(int *col)
{
    (*col)++;
    return getchar();
}

static inline void undochar(int c, int *col)
{
    (*col)--;
    ungetc(c, stdin);
}

static void MyScanf(char input[], int n)
{
    for(int i = 0; i < n -1 ; i++)
    {
        int c = getchar();
        if(c != ' ' && c != '\n' && c != EOF)
        {
            input[i] = (char) c;
        }
        else
        {
            ungetc(c, stdin);
            input[i]='\0';
            return;
        }
    }
    input[n - 1] = '\0';
}

static void ParseCommand(int r)
{
    char input[MAX_COMMAND_LENGTH];
    MyScanf(input, MAX_COMMAND_LENGTH);
    if (0 == strcmp(input, ZERO))
    {
        int c = getchar();
        if (c != '\n')
        {
            PrintErrWrongCommand(r);
            ClearLine();
        } else
        {
            Zero();
        }
    } else if (0 == strcmp(input, IS_COEFF))
    {
        int c = getchar();
        if (c != '\n')
        {
            PrintErrWrongCommand(r);
            ClearLine();
        } else if (StackIsEmpty())
        {
            PrintErrStackUnderflow(r);
        } else
        {
            printf("%d\n", IsCoeff());
        }
    } else if (0 == strcmp(input, IS_ZERO))
    {
        int c = getchar();
        if (c != '\n')
        {
            PrintErrWrongCommand(r);
            ClearLine();
        } else if (StackIsEmpty())
        {
            PrintErrStackUnderflow(r);
        } else
        {
            printf("%d\n", IsZero());
        }
    } else if (0 == strcmp(input, CLONE))
    {
        int c = getchar();
        if (c != '\n')
        {
            PrintErrWrongCommand(r);
            ClearLine();
        } else if (StackIsEmpty())
        {
            PrintErrStackUnderflow(r);
        } else
        {
            Clone();
        }
    } else if (0 == strcmp(input, ADD))
    {
        int c = getchar();
        if (c != '\n')
        {
            PrintErrWrongCommand(r);
            ClearLine();
        } else if (!StackHasTwoEl())
        {
            PrintErrStackUnderflow(r);
        } else
        {
            Add();
        }
    } else if (0 == strcmp(input, MUL))
    {
        int c = getchar();
        if (c != '\n')
        {
            PrintErrWrongCommand(r);
            ClearLine();
        } else if (!StackHasTwoEl())
        {
            PrintErrStackUnderflow(r);
        } else
        {
            Mul();
        }
    } else if (0 == strcmp(input, NEG))
    {
        int c = getchar();
        if (c != '\n')
        {
            PrintErrWrongCommand(r);
            ClearLine();
        } else if (StackIsEmpty())
        {
            PrintErrStackUnderflow(r);
        } else
        {
            Neg();
        }
    } else if (0 == strcmp(input, SUB))
    {
        int c = getchar();
        if (c != '\n')
        {
            PrintErrWrongCommand(r);
            ClearLine();
        } else if (!StackHasTwoEl())
        {
            PrintErrStackUnderflow(r);
        } else
        {
            Sub();
        }
    } else if (0 == strcmp(input, IS_EQ))
    {
        int c = getchar();
        if (c != '\n')
        {
            PrintErrWrongCommand(r);
            ClearLine();
        } else if (!StackHasTwoEl())
        {
            PrintErrStackUnderflow(r);
        } else
        {
            printf("%d\n", IsEq());
        }
    } else if (0 == strcmp(input, DEG))
    {
        int c = getchar();
        if (c != '\n')
        {
            PrintErrWrongCommand(r);
            ClearLine();
        } else if (StackIsEmpty())
        {
            PrintErrStackUnderflow(r);
        } else
        {
            printf("%d\n", Deg());
        }
    } else if (0 == strcmp(input, DEG_BY))
    {
        int c = getchar();
        if (c != ' ')
        {
            PrintErrWrongVariable(r);
            if (c != '\n')
            {
                ClearLine();
            }
        } else
        {
            bool minus = false;
            unsigned idx = 0;
            c = getchar();
            if (c == '-')
            {
                minus = true;
                c = getchar();
            }
            if (c == '\n')
            {
                PrintErrWrongVariable(r);
                return;
            }
            while (c >= '0' && c <= '9')
            {
                if (idx * 10 + (c - '0') < idx)
                {
                    PrintErrWrongVariable(r);
                    ClearLine();
                    return;
                } else
                {
                    idx = idx * 10 + (c - '0');
                    if (minus && idx != 0)
                    {
                        PrintErrWrongVariable(r);
                        ClearLine();
                        return;
                    }
                }
                c = getchar();
            }
            if (c != '\n')
            {
                PrintErrWrongVariable(r);
                ClearLine();
                return;
            }
            if (StackIsEmpty())
            {
                PrintErrStackUnderflow(r);
            } else
            {
                printf("%d\n", DegBy(idx));
            }
        }

    } else if (0 == strcmp(input, AT))
    {
        int c = getchar();
        if (c != ' ')
        {
            PrintErrWrongValue(r);
            if (c != '\n')
            {
                ClearLine();
            }
        } else
        {
            bool minus = false;
            long x = 0;
            c = getchar();
            if (c == '-')
            {
                minus = true;
                c = getchar();
            }
            if (c == '\n')
            {
                PrintErrWrongValue(r);
                return;
            }
            while (c >= '0' && c <= '9')
            {
                if (minus)
                {
                    if (x * 10 - (c - '0') > x)
                    {
                        PrintErrWrongValue(r);
                        ClearLine();
                        return;
                    } else
                    {
                        x = x * 10 - (c - '0');
                    }
                } else
                {
                    if (x * 10 + (c - '0') < x)
                    {
                        PrintErrWrongValue(r);
                        ClearLine();
                        return;
                    } else
                    {
                        x = x * 10 + (c - '0');
                    }
                }
                c = getchar();
            }
            if (c != '\n')
            {
                PrintErrWrongValue(r);
                ClearLine();
                return;
            }
            if (StackIsEmpty())
            {
                PrintErrStackUnderflow(r);
            } else
            {
                At(x);
            }
        }
    } else if (0 == strcmp(input, PRINT))
    {
        int c = getchar();
        if (c != '\n')
        {
            PrintErrWrongCommand(r);
            ClearLine();
        } else if (StackIsEmpty())
        {
            PrintErrStackUnderflow(r);
        } else
        {
            Print();
        }
    } else if (0 == strcmp(input, POP))
    {
        int c = getchar();
        if (c != '\n')
        {
            PrintErrWrongCommand(r);
            ClearLine();
        } else if (StackIsEmpty())
        {
            PrintErrStackUnderflow(r);
        } else
        {
            Pop();
        }
    } else
    {
        PrintErrWrongCommand(r);
        ClearLine();
    }
}

static poly_coeff_t ParseCoeff(int r, int *col, bool *err)
{
//    printf("Parsing coeff...\n");
    bool minus = false;
    poly_coeff_t coeff = 0;
    int c = nextchar(col);
    if (c == '-')
    {
        minus = true;
        c = nextchar(col);
    }
    while (c >= '0' && c <= '9')
    {
        if (minus)
        {
            if (coeff * 10 - (c - '0') > coeff)
            {
                PrintErr(r, *col);
                *err = true;
                undochar(c, col);
                return 0;
            } else
            {
                coeff = coeff * 10 - (c - '0');
            }
        } else
        {
            if (coeff * 10 + (c - '0') < coeff)
            {
                PrintErr(r, *col);
                *err = true;
                undochar(c, col);
                return 0;
            } else
            {
                coeff = coeff * 10 + (c - '0');
            }
        }
        c = nextchar(col);
    }
    undochar(c, col);
    return coeff;
}

static poly_exp_t ParseExp(int r, int *col, bool *err)
{
//    printf("Parsing exp...\n");
    bool minus = false;
    poly_exp_t exp = 0;
    int c = nextchar(col);
    if (c == '-')
    {
        minus = true;
        c = nextchar(col);
    }
    while (c >= '0' && c <= '9')
    {
        if (exp * 10 + (c - '0') < exp)
        {
            PrintErr(r, *col);
            *err = true;
            undochar(c, col);
            return 0;
        } else
        {
            exp = exp * 10 + (c - '0');
            if (minus && exp != 0)
            {
                PrintErr(r, *col);
                *err = true;
                undochar(c, col);
                return 0;
            }
        }
        c = nextchar(col);
    }
    undochar(c, col);
    return exp;
}

static Mono ParseMono(int r, int *col, bool *err);

static void AddMono(Mono m, MonoList *monos, unsigned *num_of_el)
{
    MonoList *ml = (MonoList *) malloc(sizeof(MonoList));
    ml->next = monos->next;
    ml->mono = m;
    monos->next = ml;
    (*num_of_el)++;
}

static Poly ParsePoly(int r, int *col, bool *err)
{
//    printf("Parsing poly...\n");
    int c = nextchar(col);
    unsigned num_of_el = 0;
    bool read_mono = true;

    MonoList *ml = (MonoList *) malloc(sizeof(MonoList));
    ml->next = NULL;

    while (c != '\n' && c != EOF && c != ',')
    {
        if (read_mono)
        {
            if (c == '-' || (c >= '0' && c <= '9'))
            {
                undochar(c, col);
                poly_coeff_t coeff = ParseCoeff(r, col, err);
                if (*err)
                {
                    for (unsigned i = 0; i < num_of_el; i++)
                    {
                        MonoList *next = ml->next->next;
                        free(ml->next);
                        ml->next = next;
                    }
                    free(ml);
                    return PolyZero();
                }
                Poly p = PolyFromCoeff(coeff);
                //            Add Mono
                AddMono(MonoFromPoly(&p, 0), ml, &num_of_el);
            } else
            {
                undochar(c, col);
                Mono m = ParseMono(r, col, err);
                if (*err)
                {
                    for (unsigned i = 0; i < num_of_el; i++)
                    {
                        MonoList *next = ml->next->next;
                        free(ml->next);
                        ml->next = next;
                    }
                    free(ml);
                    MonoDestroy(&m);
                    return PolyZero();
                }
                //            AddMono
                AddMono(m, ml, &num_of_el);
            }
        } else
        {
            for (unsigned i = 0; i < num_of_el; i++)
            {
                MonoList *next = ml->next->next;
                free(ml->next);
                ml->next = next;
            }
            free(ml);
            PrintErr(r, *col);
            *err = true;
            undochar(c, col);
            return PolyZero();
        }
        read_mono = false;
        c = nextchar(col);
        if (c == '+')
        {
            c = nextchar(col);
            read_mono = true;
        }
    }
    if (read_mono)
    {
        for (unsigned i = 0; i < num_of_el; i++)
        {
            MonoList *next = ml->next->next;
            free(ml->next);
            ml->next = next;
        }
        free(ml);
        PrintErr(r, *col);
        *err = true;
        undochar(c, col);
        return PolyZero();
    }
    undochar(c, col);
    Mono monos[num_of_el];
    for (unsigned i = 0; i < num_of_el; i++)
    {
        monos[i] = ml->next->mono;
        MonoList *next = ml->next->next;
        free(ml->next);
        ml->next = next;
    }
    free(ml);

    Poly p = PolyAddMonos(num_of_el, monos);
    return p;
}

static Mono ParseMono(int r, int *col, bool *err)
{
//    printf("Parsing mono...\n");
    int c = nextchar(col);
    if (c != '(')
    {
//        printf("%d err near brace\n", r);
        PrintErr(r, *col);
        *err = true;
        undochar(c, col);
        return (Mono) {.p = PolyZero(), .exp = 0};
    }
    // poly
    Poly p = ParsePoly(r, col, err);
    if (*err)
    {
        PolyDestroy(&p);
        return (Mono) {.p = PolyZero(), .exp = 0};
    }
    // comma
    c = nextchar(col);
    if (c != ',')
    {
        PrintErr(r, *col);
        *err = true;
        undochar(c, col);
        return MonoFromPoly(&p, 0);
    }
    // coefficient
    c = nextchar(col);
    if (!(c >= '0' && c <= '9'))
    {
        PrintErr(r, *col);
        *err = true;
        undochar(c, col);
        return MonoFromPoly(&p, 0);
    }
    undochar(c, col);
    poly_exp_t exp = ParseExp(r, col, err);
    if (*err)
    {
        return MonoFromPoly(&p, exp);
    }
    // closing brace
    c = nextchar(col);
    if (c != ')')
    {
        PrintErr(r, *col);
        *err = true;
        undochar(c, col);
        return MonoFromPoly(&p, exp);
    }
    return MonoFromPoly(&p, exp);
}
static void ParsePolynomial(int r)
{
    int c = getchar();
    if (c == '\n')
    {
        PrintErr(r, 1);
        return;
    } else
    {
        ungetc(c, stdin);
    }

    int col = 1;
    bool error = false;
    Poly p = ParsePoly(r, &col, &error);
    if (error)
    {
        PolyDestroy(&p);
        ClearLine();
        return;
    }
    c = nextchar(&col);
    if (c != '\n' && c != EOF)
    {
        PrintErr(r, col);
        PolyDestroy(&p);
        ClearLine();
    } else
    {
        StackPush(p);
    }
}

int main()
{
    int row = 1;
    stack = NULL;
    int c = getchar();
    while (c != EOF)
    {
        if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
        {
            ungetc(c, stdin);
            ParseCommand(row);
        } else
        {
            ungetc(c, stdin);
            ParsePolynomial(row);
        }
        row++;
        c = getchar();
    }
    StackFree();
    return 0;
}